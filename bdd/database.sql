CREATE TABLE Administrateur (
    id SERIAL PRIMARY KEY,
    username VARCHAR(200) NOT NULL,
    mdp VARCHAR(200) NOT NULL
);

CREATE TABLE TypeProduit(
    id SERIAL PRIMARY KEY,
    nom VARCHAR(200) NOT NULL
);

CREATE TABLE Produit (
    id SERIAL PRIMARY KEY,
    titre VARCHAR(100) NOT NULL,
    visuel TEXT NOT NULL,
    body TEXT,
    idtypeproduit INT REFERENCES TypeProduit(id),
    date1 DATE NOT NULL,
    date2 DATE,
    lieu TEXT
);

INSERT INTO Administrateur VALUES
(DEFAULT,'admin','admin');

INSERT INTO TypeProduit VALUES
(DEFAULT,'article'),
(DEFAULT,'evenement');

