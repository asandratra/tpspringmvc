psql -U postgres
CREATE DATABASE tp_spring;
CREATE ROLE tp_spring LOGIN PASSWORD 'tp_spring';
ALTER DATABASE tp_spring OWNER TO tp_spring;
\q

psql -U tp_spring
