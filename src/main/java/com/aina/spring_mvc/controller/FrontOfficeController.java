package com.aina.spring_mvc.controller;

import com.aina.spring_mvc.model.Produit;
import com.aina.HibernateDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class FrontOfficeController {

    @Autowired
    HibernateDao dao;

    @GetMapping({"/articles/{page}", "/articles/{page}/{recherche}"})
    public String articles(@PathVariable(name="page")Integer page,@PathVariable(name="recherche", required=false) @RequestParam(name="recherche",required = false) String recherche, Model model){
        Produit p = new Produit();
        if(recherche!=null && !recherche.equals("")){
            p.setTitre(recherche);
            //p.setBody(recherche);
            model.addAttribute("page",page);
            model.addAttribute("recherche",recherche);
            model.addAttribute("articles",dao.paginateWhere(p, (page-1)*3, 3));
            return "articles";
        }
        model.addAttribute("page",page);
        model.addAttribute("recherche","");
        model.addAttribute("articles",dao.paginate(Produit.class, (page-1)*3, 3));
        return "articles";
    }

    public HibernateDao getDao() {
        return dao;
    }

    public void setDao(HibernateDao dao) {
        this.dao = dao;
    }
}
