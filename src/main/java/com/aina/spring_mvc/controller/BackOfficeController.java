package com.aina.spring_mvc.controller;

import com.aina.spring_mvc.model.Administrateur;
import com.aina.spring_mvc.model.Produit;
import com.aina.spring_mvc.model.TypeProduit;
import com.aina.HibernateDao;

import java.io.File;
import java.io.OutputStream;
import java.nio.file.Files;
import java.sql.Date;
import java.util.Base64;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartResolver;

@Controller
public class BackOfficeController implements ServletContextAware{

    @Autowired
    HibernateDao dao;

    @Autowired
    ServletContext context;

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        return new MultipartConfigElement("");
    }

    @Bean
    public MultipartResolver multipartResolver() {
        org.springframework.web.multipart.commons.CommonsMultipartResolver multipartResolver = new org.springframework.web.multipart.commons.CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(1000000);
        return multipartResolver;
    }

    @GetMapping("/form-login")
    public String toLogin(Model model){
        return "login";
    }

    @PostMapping("/login")
    public String login(@ModelAttribute Administrateur ad, Model model){
        Administrateur a = null;
        a = (Administrateur) dao.findWhere(ad).get(0);
        String re;
        if(a==null){
            re="login";
            model.addAttribute("message","Erreur d'authentification");
        }
        else{
            model.addAttribute("typeProduits", dao.findAll(TypeProduit.class) );
            re="pre";
        }
        return re;
    }

    @GetMapping("/form-produit")
    public String formulaireProduit(Model model){
        return "main";
    }
    

    @GetMapping("/form-article")
    public String formulaireArticle(Model model){
        return "main1";
    }

    @PostMapping(value="/produit", consumes=MediaType.MULTIPART_FORM_DATA_VALUE)
    public String createProduit(@RequestParam(name="titre") String titre, @RequestParam(name="visuel") String image, @RequestParam(name="body") String body, @RequestParam(name="idtypeproduit") Integer idtypeproduit, @RequestParam(name="date1") Date date1, @RequestParam(name="date2") Date date2, @RequestParam(name="lieu") String lieu, Model model){
        Produit p;
        try{
            p=new Produit();/* 
            String filename = image.getOriginalFilename();
            System.out.println(filename);
            String relpath = context.getRealPath("/resources/theme/");
            File file = new File(relpath+filename);
            image.transferTo(file); */
            p.setTitre(titre);
            p.setVisuel(image);
            p.setBody(body);
            p.setIdtypeproduit(idtypeproduit);
            p.setDate1(date1);
            p.setDate2(date2);
            p.setLieu(lieu);
            dao.create(p);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return "pre";
    }
    @PostMapping(value="/article", consumes=MediaType.MULTIPART_FORM_DATA_VALUE)
    public String createArticle(@RequestParam(name="titre") String titre, @RequestParam(name="visuel") String image, @RequestParam(name="body") String body, @RequestParam(name="idtypeproduit") Integer idtypeproduit, @RequestParam(name="date1") Date date1, @RequestParam(name="lieu") String lieu, Model model){
        Produit p;
        try{
            p=new Produit();/* 
            String filename = image.getOriginalFilename();
            System.out.println(filename);
            String relpath = context.getRealPath("/resources/theme/");
            File file = new File(relpath+filename);
            image.transferTo(file); */
            p.setTitre(titre);
            p.setVisuel(image);
            p.setBody(body);
            p.setIdtypeproduit(idtypeproduit);
            p.setDate1(date1);
            p.setLieu(lieu);
            dao.create(p);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return "pre";
    }

    public HibernateDao getDao() {
        return dao;
    }

    public void setDao(HibernateDao dao) {
        this.dao = dao;
    }

    @Override
    public void setServletContext(ServletContext servletContext) {
        // TODO Auto-generated method stub
        
    }
}
