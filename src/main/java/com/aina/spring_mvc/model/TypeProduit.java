package com.aina.spring_mvc.model;

import javax.persistence.*;

@Entity
public class TypeProduit{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String nom;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String u) {
        this.nom = u;
    }
}
