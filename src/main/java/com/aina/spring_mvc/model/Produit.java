package com.aina.spring_mvc.model;

import javax.persistence.*;
import java.sql.*;

@Entity
public class Produit{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String titre;

    private String visuel;

    private String body;

    private Integer idtypeproduit;

    private Date date1;

    private Date date2;

    private String lieu;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String u) {
        this.titre = u;
    }

    public String getVisuel() {
        return visuel;
    }

    public void setVisuel(String m) {
        this.visuel = m;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String u) {
        this.body = u;
    }

    public Integer getIdtypeproduit() {
        return idtypeproduit;
    }

    public void setIdtypeproduit(Integer id) {
        this.idtypeproduit = id;
    }

    public Date getDate1() {
        return date1;
    }

    public void setDate1(Date m) {
        this.date1 = m;
    }

    public Date getDate2() {
        return date2;
    }

    public void setDate2(Date m) {
        this.date2 = m;
    }

    public void setLieu(String u) {
        this.lieu = u;
    }

    public String getLieu() {
        return lieu;
    }

    public String getRelPath(){
        visuel = visuel.replace("\\", "/");
        String[] rep = visuel.split("/");
        return "/"+rep[rep.length - 3 ]+"/"+rep[rep.length - 2 ]+"/"+rep[rep.length - 1 ];
    }
}
