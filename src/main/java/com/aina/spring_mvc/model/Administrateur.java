package com.aina.spring_mvc.model;

import javax.persistence.*;

@Entity
public class Administrateur{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String username;

    private String mdp;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String u) {
        this.username = u;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String m) {
        this.mdp = m;
    }
}
