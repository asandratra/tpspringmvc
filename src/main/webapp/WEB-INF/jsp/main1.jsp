<%@ page import="com.aina.spring_mvc.model.TypeProduit" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <jsp:include page="resources/theme/refactor/head.jsp" />
<body>
    <jsp:include page="resources/theme/refactor/navbar.jsp" />
<main class="page registration-page">
    <section class="clean-block clean-form dark">
        <div class="container">
            <div class="block-heading">
                <h2 class="text-info">Ajout d'article</h2>
            </div>
            <form action="<%= request.getContextPath() %>/article" method="post" enctype="multipart/form-data">
                <div class="mb-3"><label class="form-label" for="name">Titre</label><input class="form-control" type="text" name="titre"></div>
                <div class="mb-3"><label class="form-label" for="name">Visuel</label><input class="form-control" type="file" id="selectimage"></div>
                <input type="hidden" name="visuel" id="upload">
                <div class="mb-3"><label class="form-label" for="name">Corps</label><textarea class="form-control" name="body" cols="30" rows="10"></textarea></div>
                <input type="hidden" value="1" name="idtypeproduit">
                <div class="mb-3"><label class="form-label" for="name">Date</label><input class="form-control" type="date" name="date1"></div>
                <div class="mb-3"><label class="form-label" for="name">Lieu</label><textarea class="form-control" name="lieu" cols="30" rows="10"></textarea></div>
                <button class="btn btn-primary" type="submit">Confirmer</button>
                </div>
            </form>
        </div>
    </section>
</main>

<jsp:include page="resources/theme/refactor/footer.jsp" />
</body>
<script type="text/javascript">
    const input = document.getElementById("selectimage");
    const visuel = document.getElementById("upload");
    const convertBase64 = (file) => {
        return new Promise((resolve,reject) => {
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file);

            fileReader.onload = () => {
                resolve(fileReader.result);
            };

            fileReader.onerror = (error) => {
                reject(error);
            };
        });
    };
    const uploadImage = async (event) => {
        const file = event.target.files[0];
        const base64 = await convertBase64(file);
        visuel.value = base64;
        console.log(visuel.value);
    };
    input.addEventListener("change", (e) => {
        uploadImage(e);
    });
</script>
<jsp:include page="resources/theme/refactor/script.jsp" />
</html>
