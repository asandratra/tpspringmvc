<%@ page language="java" contentType="text/html" %>
<%@ page import="java.util.*" %>

<!DOCTYPE html>
<html lang="en">
    <jsp:include page="resources/theme/refactor/head.jsp" />
<body>
    <jsp:include page="resources/theme/refactor/navbar.jsp" />
    <main class="page landing-page">
        <section class="clean-block clean-hero" style="color:rgba(9, 162, 255, 0.85);">
            <div class="text">
                <h2>Ajout d'Articles</h2>
                <a href="form-article"><button class="btn btn-outline-light btn-lg" type="button">Article</button></a>
                <a href="form-produit"><button class="btn btn-outline-light btn-lg" type="button">evenement</button></a>
            </div>
        </section>
    </main>
    <jsp:include page="resources/theme/refactor/footer.jsp"/>
</body>
<jsp:include page="resources/theme/refactor/script.jsp" />
</html>