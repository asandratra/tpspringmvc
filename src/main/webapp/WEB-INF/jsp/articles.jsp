<%@ page import="com.aina.spring_mvc.model.Produit" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <jsp:include page="resources/theme/refactor/head.jsp" />
<%
  List<Produit> produits = (List<Produit>) request.getAttribute("articles");
    int p = Integer.parseInt(request.getAttribute("page").toString());
    String recherche = request.getAttribute("recherche").toString();
%>
<body>
  <jsp:include page="resources/theme/refactor/navbar.jsp" />
<main class="page gallery-page">
  <section class="clean-block clean-gallery dark">
      <div class="container">
          <div class="block-heading">
              <h2 class="text-info">Articles</h2>
              <p>
                <form action="<%= request.getContextPath() %>/articles/1" method="get">
                <div class="mb-3"><input class="form-control" type="text" name="recherche" placeholder="Recherche"></div>
                <button class="btn btn-primary" type="submit">Rechercher</button>
                </div>
                </form>
                <%
                if(p>1){
                    %><div style="margin-left: 0; margin-right: auto;"><a href="<%= request.getContextPath() %>/articles/<%= p-1 %>/<%= recherche %>"><< Previous</a></div><%
                }
                %>
                <%
                if(produits.size()>=3){
                    %><div style="margin-left: auto; margin-right: 0;"></div><a href="<%= request.getContextPath() %>/articles/<%= p+1 %>/<%= recherche %>">Next >></a><%
                }
                %>
              </p>
          </div>
          <div class="row" style="justify-content: center;">
              <div class="col-md-9">
                  <div class="row" id="galerie">
                      <%
                      if(produits.size()<=0){
                      %>
                      <p style="text-align: center;color: gray;">Rien de plus, veuillez retourner a la page precedente</p>
                      <%
                      } 
                      for (Produit produit : produits) { %>
                        <div class='col-md-6 col-lg-4 item'><img class='img-thumbnail img-fluid image' src="<%= produit.getVisuel() %>" style="height: auto;width: 100%;object-fit: contain;"><p style="text-align: center;"><%= produit.getTitre() %></p></div>
                      <%  } %>
                  </div>
              </div>
          </div>
      </div>
  </section>
</main>
<jsp:include page="resources/theme/refactor/footer.jsp" />
</body>
<jsp:include page="resources/theme/refactor/script.jsp" />
</html>
