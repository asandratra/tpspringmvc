<%@ page language="java" contentType="text/html" %>
<%@ page import="java.util.*" %>

<!DOCTYPE html>
<html lang="en">
    <jsp:include page="resources/theme/refactor/head.jsp" />
<body>
    <main class="page landing-page">
        <section class="clean-block clean-hero" style="color:rgba(9, 162, 255, 0.85);">
            <div class="text">
                <h2>Articles</h2>
                <a href="form-login"><button class="btn btn-outline-light btn-lg" type="button">Back</button></a>
                <a href="articles/1"><button class="btn btn-outline-light btn-lg" type="button">Front</button></a>
            </div>
        </section>
    </main>
    <jsp:include page="resources/theme/refactor/footer.jsp"/>
</body>
<jsp:include page="resources/theme/refactor/script.jsp" />
</html>