<%@ page language="java" contentType="text/html" %>
<%@ page import="java.util.*" %>
<%@ page import="model.OeuvreModel" %>
<%@ page import="model.UtilisateurModel" %>
<%@ page import="model.CommissionModel" %>
<%@ page import="model.ThemeModel" %>

<%
ServletContext context = request.getServletContext();

UtilisateurModel user = (UtilisateurModel) session.getAttribute("user");

ArrayList arr = (ArrayList) context.getAttribute("fullGallery");
ArrayList themes = (ArrayList) context.getAttribute("themes");
int i;
int u;
ThemeModel[] tm = new ThemeModel[themes.size()];
for(u=0;u<themes.size();u++){
    tm[u] = (ThemeModel) themes.get(u);
}
OeuvreModel[] om = new OeuvreModel[arr.size()];
for(u=0;u<arr.size();u++){
    om[u] = (OeuvreModel) arr.get(u);
}
%>   
    <script type="text/javascript">
        function filtre(){
            var themes = $('input[name="theme"]:checked').serialize();
            var tab = document.getElementsByClassName('titre');
            var titre = [...tab].map(input => input.value);
            $.post('gallery',
            {
                ftheme: themes,
                ftitre: titre[0]
            },
                function(responseText){
                    $("#galerie").html(responseText);
            });
        }
    </script>
    <main class="page gallery-page">
        <section class="clean-block clean-gallery dark">
            <div class="container">
                <div class="block-heading">
                    <h2 class="text-info">Gallerie d'Exposition</h2>
                    <p>Admirez et Appreciez</p>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="d-none d-md-block">
                            <div class="filters">
                                <div class="filter-item">
                                    <form>
                                        <h3>Themes</h3>
                                        <%
                                        for(i=0;i<tm.length;i++){
                                            out.println("<div class='form-check'><input class='themes' type='checkbox' name='theme' value="+tm[i].getId()+" onchange='filtre()'><label class='form-check-label'>"+tm[i].getNom()+"</label></div>");
                                        }
                                        %>
                                    </form>
                                </div>
                                <div class="filter-item">
                                    <form>
                                        <h3>Titres</h3><input class="titre" type="text" placeholder="Titre d'oeuvre" onkeyup="filtre()" >
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="row" id="galerie">
                            <%
                            for(i=0;i<om.length;i++){
                                out.println("<div class='col-md-6 col-lg-4 item'><a class='lightbox' href='detailsImage?oeuvre="+om[i].getId()+"&&previous=gallery'><img class='img-thumbnail img-fluid image' src='"+om[i].getTomPath()+"'></a></div>");
                            }
                            %>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>